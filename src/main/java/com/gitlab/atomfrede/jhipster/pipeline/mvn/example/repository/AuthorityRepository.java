package com.gitlab.atomfrede.jhipster.pipeline.mvn.example.repository;

import com.gitlab.atomfrede.jhipster.pipeline.mvn.example.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
