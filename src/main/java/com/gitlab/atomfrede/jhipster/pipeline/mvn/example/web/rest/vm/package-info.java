/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gitlab.atomfrede.jhipster.pipeline.mvn.example.web.rest.vm;
